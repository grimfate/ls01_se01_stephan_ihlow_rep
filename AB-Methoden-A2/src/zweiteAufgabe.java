import java.util.Scanner;

public class zweiteAufgabe {
	static Scanner myScanner = new Scanner(System.in);
	public static void main(String[] args) {
		double nettogesamtpreis;
		double bruttogesamtpreis;
		// Benutzereingaben lesen
		
		String artikel = liesString("was m�chten Sie bestellen?");
		
		int anzahl = liesInt("Geben Sie die Anzahl ein:");
	
		double preis = liesDouble("Geben Sie den Nettopreis ein:");
	
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		
		nettogesamtpreis = berechneGesamtnettopreis(anzahl,preis);
		bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
		
	}
	public static String liesString(String text) {
		System.out.println(text);
		return myScanner.next();
	}	
	public static int liesInt(String text) {
		System.out.println(text);
		return myScanner.nextInt();
	
	}
	public static double liesDouble(String text) {
		System.out.println(text);
		return myScanner.nextDouble();
	}
	public static double berechneGesamtnettopreis(int anzahl, double preis) {
		double nettogesamtpreis = anzahl * preis;
		return nettogesamtpreis;
	}
	public static double berechneGesamtbruttopreis(double bruttogesamtpreis, double mwst) {
		return bruttogesamtpreis * (1+mwst/100);
		
	}
	public static void rechnungausgeben(String artikel,int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
	System.out.println("\tRechnung");
	System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
	System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		
	}
}


