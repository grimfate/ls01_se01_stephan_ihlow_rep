import java.util.Scanner;

public class dritteAufgabe {
	
	static Scanner resistance = new Scanner(System.in);

	public static void main(String[] args) {
		double r1 = liesR1("Bitte geben Sie R1 an: ");
		
		double r2 = liesR2("Bitte geben Sie R2 an: ");
		
		double rg = reihenschaltung(r1,r2);
		
		printResistance(rg);
				
	}
	
	public static double liesR1(String text) {
		System.out.println(text);
		return resistance.nextDouble();
	}
	
	public static double liesR2(String text) {
		System.out.println(text);
		return resistance.nextDouble();
	}
	
	public static double reihenschaltung(double r1, double r2){
		double rg = r1 + r2 ;
		return rg;
	}
	
	public static void printResistance(double rg) {
		System.out.println("Der Ersatzwiderstand in Reihenschaltung betr�gt: " + rg + " Ohm.");
				
	}
}
