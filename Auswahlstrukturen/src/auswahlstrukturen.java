import java.util.Scanner;
public class auswahlstrukturen {

	public static Scanner myScanner = new Scanner (System.in); 
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//aufgabe11();
		//aufgabe12();
		//aufgabe13();
		//aufgabe14();
		//aufgabe15();
		//aufgabe16();
		//aufgabe17();
		//aufgabe2steuersatz();
		//aufgabe3hardware();
		//aufgabe4rabatt();
		//aufgabe5bmi();
		aufgabe7sortieren();
	
	}

	
	public static void aufgabe11() {
		System.out.println("Bitte geben Sie zwei Ganzzahlen ein: ");
		int a = myScanner.nextInt();
		int b = myScanner.nextInt();
		System.out.print("Wenn die T�r verschlossen ist, ");
		if(a>b) {
			System.out.println("ist sie verschlossen.");
		}
		else {
			System.out.println("braucht man einen Schl�ssel");
		}
		
	}
	public static void aufgabe12() {
		System.out.println("Bitte geben Sie zwei Ganzzahlen ein: ");
		int a = myScanner.nextInt();
		int b = myScanner.nextInt();
		if(a==b) {
			System.out.println("Die Zahlen sind gleichgro�.");
			System.out.println();
		}
		
		
	}
	public static void aufgabe13() {
		System.out.println("Bitte geben Sie zwei Ganzzahlen ein: ");
		int a = myScanner.nextInt();
		int b = myScanner.nextInt();
		if (b>a) {
			System.out.println("Zahl 2 ist gr��er als Zahl 1");
			System.out.println();
		}
	}
	public static void aufgabe14() {
		System.out.println("Bitte geben Sie zwei Ganzzahlen ein: ");
		int a = myScanner.nextInt();
		int b = myScanner.nextInt();
		if(a>=b) {
			System.out.println("Zahl 1 ist gr��er gleich Zahl 2.");
			System.out.println();
		}
		else {
			System.out.println("Zahl 1 ist nicht gr��er gleich Zahl 2.");
		}
		
	}
	public static void aufgabe15() {
		System.out.println("Bitte geben Sie drei Ganzzahlen ein: ");
		int a = myScanner.nextInt();
		int b = myScanner.nextInt();
		int c = myScanner.nextInt();
		if((a>b) && (a>c)) {
			System.out.println("Zahl 1 ist gr��er als Zahl 2 und Zahl 3.");
		}		
	}
	public static void aufgabe16() {
		System.out.println("Bitte geben Sie drei Ganzzahlen ein: ");
		int a = myScanner.nextInt();
		int b = myScanner.nextInt();
		int c = myScanner.nextInt();
		if ((c>b)|| (c>a)) {
			System.out.println("Zahl 3 ist gr��er als Zahl 1 oder gr��er als Zahl 2");
			}	
		}
	public static void aufgabe17() {
		System.out.println("Bitte geben Sie drei Ganzzahlen ein: ");
		int a = myScanner.nextInt();
		int b = myScanner.nextInt();
		int c = myScanner.nextInt();
		if((a>b)&&(a>c)) {System.out.println("Zahl 1 ist die gr��te der drei Zahlen");}
		if((b>a)&&(b>c)) {System.out.println("Zahl 2 ist die gr��te der drei Zahlen");}
		if((c>a)&&(c>b)) {System.out.println("Zahl 3 ist die gr��te der drei Zahlen");}
	}

	public static void aufgabe2steuersatz() {
		System.out.println("Bitte geben Sie den zu versteuernden Betrag an: ");
		double a = myScanner.nextDouble();
		System.out.println("Soll der erm��igte Steuersatz angewendet werden? j/n: ");
		if(myScanner.hasNext("j")||(myScanner.hasNext("J"))) {
			double b = a * 0.93;
			System.out.println("Der Bruttobetrag bel�uft sich auf: " + b + "�");
		}
		else {
			double b = a * 0.81; 
			System.out.println("Der Bruttobetrag bel�uft sich auf " + b + "�");
		}
	}
	
	public static void aufgabe3hardware() {
		System.out.println("bitte geben Sie die Bestellmenge an: ");
		int a = myScanner.nextInt();
		System.out.println("Bitte geben Sie den Einzelpreis ( Netto) an: ");
		double b = myScanner.nextInt();
		int c = 10;
		double d = a * b * 1.19;
		if(a>=c) {
			System.out.println("Der Rechnungsbetrag bel�uft sich auf: " +d + "�." );
		}
		else {
			System.out.println("Der Rechnungsbetrag bel�uft sich auf: " + (d+10) + "�");
		}
		
	}
	
	public static void aufgabe4rabatt() {
		System.out.println("Bitte geben Sie den Bestellwert an: ");
		double a = myScanner.nextDouble();
		int b = 0;
		int c = 100;
		int d = 500;
		if((a>b)&&(a<=c)){
			
			System.out.printf("%s%.2f%s" ,"Der Bestellwert f�llt in Kategorie 1  und betr�gt ", a*0.9 , "�." );
		}
		if((a>c)&&(a<=d)){
			System.out.printf("%s%.2f%s", "Der Bestellwert f�llt in Kategorie 2 und betr�gt ", a*0.85, "�." );
		}
		if(a>d){
			System.out.printf("%s%.2f%s", "Der Bestellwert f�llt in Kategorie 3 und betr�gt ", a*0.8 , "�." );
		}
		
		
	}

	public static void aufgabe5bmi() {
		System.out.println("Bitte geben Sie Ihre K�rpergr��e in Meter an: ");
		double a = myScanner.nextDouble();
		System.out.println("Bitte geben Sie Ihr Gewicht Kilo an: ");
		double b = myScanner.nextDouble();
		double c = b/(a*a);
		int d = 20;
		int e = 25;
		
		System.out.println("Bitte geben Sie Ihr Geschlecht an: m/w");
		if((myScanner.hasNext("m"))||(myScanner.hasNext("M"))){
			if(c<d) {
				System.out.println("Sie haben Untergewicht.");
			}
			if((c>d)&&(c<e)) {
				System.out.println("Sie haben Normalgewicht.");
			}
			if(c>e) {
				System.out.println("Sie haben �bergewicht");
			}
		}
		else {
			if(c<(d-1)) {
				System.out.println("Sie haben Untergewicht.");
			}
			if((c>(d-1))&&(c<(e-1))) {
				System.out.println("Sie haben Normalgewicht.");
			}
			if(c>(e-1)) {
				System.out.println("Sie haben �bergewicht");
			}
		}			
		
	}

	public static void aufgabe7sortieren() {
		
		System.out.println("Bitte geben Sie 3 Zeichen ein: ");
		System.out.println("1: ");
		char a = myScanner.next().charAt(0);
		System.out.println("2: ");
		char b = myScanner.next().charAt(0);
		System.out.println("3: ");
		char c = myScanner.next().charAt(0);
		
		if((a<b) && (a<c)) {
			if(b<c) {
				System.out.println( a + ", " + b + ", " + c);
			}
			else {
				System.out.println( a + ", " + c + ", " + b);
			}
		}
		if((b<a) && (b<c)) {
			if(a<c) {
				System.out.println( b + ", " + a + ", " + c);
			}
			else {
				System.out.println( b + ", " + c + ", " + a);
			}
		}
		if((c<a) && (c<b)) {
			if(a<b) {
				System.out.println( c + ", " + a + ", " + b);
			}
			else {
				System.out.println( c + ", " + b + ", " + a);
			}
		}
		
	}



}
