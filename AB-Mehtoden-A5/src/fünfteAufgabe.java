import java.util.Scanner;

public class fünfteAufgabe {
	
	
	static Scanner doubleScanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		double b = liesBasis("Bitte geben Sie die Basis an: ");
		double e = liesExponent("Bitte geben Sie den Exponenten an: ");
		double erg = berechneErgebnis(b,e);
		printErg(b,e,erg);
		
	}
		public static double liesBasis(String text){
			System.out.println(text);
			return doubleScanner.nextDouble();
		}

		public static double liesExponent(String text){
			System.out.println(text);
			return doubleScanner.nextDouble();
		}
		public static double berechneErgebnis(double basis, double exponent) {
			double basis1 = basis;
			for (int i = 0; i < exponent-1; i++) {
				basis = basis * basis1;
			}
			return basis;
		}
		public static void printErg(double basis, double exponent, double ergebnis) {
			System.out.println(basis + " hoch " + exponent + " = " + ergebnis );
		}
	}


