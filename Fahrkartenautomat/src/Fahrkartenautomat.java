﻿import java.util.Scanner;

class Fahrkartenautomat
{
	 static Scanner tastatur = new Scanner(System.in);
    public static void main(String[] args)
    {
        String check = null;
        while (check !="n") {															//Tickets können nun erneut erworben werden
                  
       double ticketpreis = fahrkartenbestellungErfassen();
       
       //ticketanzahl erfassen
      int  check2 = 0;
       
       System.out.print("Ticketanzahl: ");				
       int ticketanzahl = 0;
       while(check2!=1){
       ticketanzahl = tastatur.nextInt();
       		if((ticketanzahl>=1)&&(ticketanzahl<=10)) {
       			check2 = 1;       			
       		}
       		else {
       			System.out.println("Ungültige Ticketanzahl! Bitte korrigieren Sie Ihre Eingabe!");
       		}
       		
       }
       ticketpreis = ticketpreis *100;
       int zuzahlenderGesamtbetrag = (int)ticketpreis * ticketanzahl;
       int restausgabe = fahrkartenbezahlen(zuzahlenderGesamtbetrag);
       
       fahrkartenAusgeben((int)ticketanzahl);
       rueckgeldAusgeben(restausgabe);
        
       System.out.println("Möchten Sie weitere Tickets buchen? 'j' für ja, 'n' für nein.");
        
       check = tastatur.next();
        }
    }
    public static double fahrkartenbestellungErfassen() {
    	int i = 0;
    	double ticketpreis = 0;
    	int check1 = 0;
    
    	System.out.println("Berlin A [1] 4,70€");
    	System.out.println("Berlin AB [2] 6,80€");
    	System.out.println("Berlin ABC [3] 12,80€");
    	System.out.println("Berlin Brandenburg Ticket [4] 15,00€");
    	System.out.println("Bitte wählen Sie ein Ticketangebot aus:");
    	while(check1!=1) {
    	i = tastatur.nextInt();
    		if((i>=1)&&(i<=4)) {
    			check1 = 1;
    		}
    		else {
    			System.out.println("Ungültige Auswahl! Bitte wählen Sie ein Ticketangebot aus!: ");
    		}
    	}
    	switch(i) {   	
    	case 1: ticketpreis = 4.7;
    	break;
    	case 2: ticketpreis = 6.8;
    	break;
    	case 3: ticketpreis = 12.8;
    	break;
    	case 4: ticketpreis = 15.0;
    	break;
    	
    	}
        return ticketpreis;
    }
    
    
    
    public static int fahrkartenbezahlen (double zuZahlen){
    	
    	double gezahlt = 0;
        while(gezahlt < zuZahlen)
        {
     	   System.out.printf("%s%.2f%s%n", "Noch zu zahlen: ", ((zuZahlen/100) - (gezahlt/100)), " Euro");
     	   System.out.println("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double eingeworfeneMünze = tastatur.nextShort();
            gezahlt += eingeworfeneMünze*100;
        }
        
    	int restausgabe = (int) (gezahlt - zuZahlen);
    	return restausgabe;
    }
    
    //Rechnungen in Integer um Rundungsfehler zu vermeiden
    
    public static void fahrkartenAusgeben (int ticketanzahl) {
    	
    	System.out.println(+ ticketanzahl + " Fahrscheine werden ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    	
    }
    
    public static void rueckgeldAusgeben (int rueckgeld) {
    	if(rueckgeld > 0)
        {
     	   System.out.printf("%s%.2f%s%n", "Der Rückgabebetrag in Höhe von ", (double) rueckgeld/100, " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rueckgeld >= 200) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
         	 rueckgeld -= 200;
            }
            while(rueckgeld >= 100) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
         	 rueckgeld -= 100;
            }
            while(rueckgeld >= 50) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
         	 rueckgeld -= 50;
            }
            while(rueckgeld >= 20) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
         	 rueckgeld -= 20;
            }
            while(rueckgeld >= 10) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
         	 rueckgeld -= 10;
            }
            while(rueckgeld >= 5)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
         	 rueckgeld -= 5;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
    	
        
      
    }

    }

